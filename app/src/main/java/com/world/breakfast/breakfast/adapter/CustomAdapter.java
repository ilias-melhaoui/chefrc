package com.world.breakfast.breakfast.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.squareup.picasso.Picasso;
import com.world.breakfast.breakfast.R;
import com.world.breakfast.breakfast.controller.DetailsActivity;
import com.world.breakfast.breakfast.controller.MainActivity;
import com.world.breakfast.breakfast.model.Breakfast;

import java.util.List;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.CustomViewHolder> {


    private final List<Breakfast> dataList;
    private Context context;

    public CustomAdapter(Context context, List<Breakfast> dataList) {
        this.context = context;
        this.dataList = dataList;
    }


    class CustomViewHolder extends RecyclerView.ViewHolder {

        public final View mView;


        public TextView name, ingredients, instructions, id, likes;
        ImageView image;
        CardView cardView;

        CustomViewHolder(View itemView) {
            super(itemView);
            mView = itemView;

            cardView = itemView.findViewById(R.id.card_view_friend);
            name = itemView.findViewById(R.id.tvName);
            ingredients = itemView.findViewById(R.id.tvIngredients);
            instructions = itemView.findViewById(R.id.tvInstructions);
            likes = itemView.findViewById(R.id.tvLikes);
            id = itemView.findViewById(R.id.tvId);
            image = itemView.findViewById(R.id.image);


        }
    }


    @Override
    public int getItemCount() {
        try {
            return dataList.size();
        }catch (Exception e){
            return 0;
        }

    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View menuItemLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.custom_row, viewGroup, false);
        return new CustomViewHolder(menuItemLayoutView);

    }


    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {


        holder.name.setText(dataList.get(position).getName());
        holder.instructions.setText(dataList.get(position).getInstructions());
        holder.ingredients.setText(dataList.get(position).getIngredients());
        holder.likes.setText(String.valueOf(dataList.get(position).getLikes()));
        holder.id.setText(String.valueOf(dataList.get(position).getId()));
        Picasso.get().load(dataList.get(position).getImage()).placeholder(R.drawable.ic_launcher_background).into(holder.image);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (MainActivity.mInterstitialAd.isLoaded()) {
                    MainActivity.mInterstitialAd.show();
                    MainActivity.mInterstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdFailedToLoad(int i) {
                            super.onAdFailedToLoad(i);
                            Intent intent = new Intent(context, DetailsActivity.class);
                            intent.putExtra("name", dataList.get(position).getName());
                            intent.putExtra("image", dataList.get(position).getImage());
                            intent.putExtra("likes", dataList.get(position).getLikes());
                            intent.putExtra("id", dataList.get(position).getId());
                            intent.putExtra("instructions", dataList.get(position).getInstructions());
                            intent.putExtra("ingredients", dataList.get(position).getIngredients());
                            context.startActivity(intent);

                        }

                        @Override
                        public void onAdLeftApplication() {
                            super.onAdLeftApplication();


                        }


                        @Override
                        public void onAdOpened() {
                            super.onAdOpened();
                        }

                        @Override
                        public void onAdLoaded() {
                            super.onAdLoaded();
                        }

                        @Override
                        public void onAdClicked() {
                            super.onAdClicked();
                        }

                        @Override
                        public void onAdImpression() {
                            super.onAdImpression();
                        }

                        @Override
                        public void onAdClosed() {
                            Intent intent = new Intent(context, DetailsActivity.class);
                            intent.putExtra("name", dataList.get(position).getName());
                            intent.putExtra("image", dataList.get(position).getImage());
                            intent.putExtra("likes", dataList.get(position).getLikes());
                            intent.putExtra("id", dataList.get(position).getId());
                            intent.putExtra("instructions", dataList.get(position).getInstructions());
                            intent.putExtra("ingredients", dataList.get(position).getIngredients());
                            context.startActivity(intent);
                        }
                    });
                } else {
                    Intent intent = new Intent(context, DetailsActivity.class);
                    intent.putExtra("name", dataList.get(position).getName());
                    intent.putExtra("image", dataList.get(position).getImage());
                    intent.putExtra("likes", dataList.get(position).getLikes());
                    intent.putExtra("id", dataList.get(position).getId());
                    intent.putExtra("instructions", dataList.get(position).getInstructions());
                    intent.putExtra("ingredients", dataList.get(position).getIngredients());
                    context.startActivity(intent);
                }


            }
        });


    }


    //this bottom part is create for search view


//    public Filter getFilter() {
//        return exampleFilter;
//    }
//
//    private Filter exampleFilter = new Filter() {
//        @Override
//        protected FilterResults performFiltering(CharSequence constraint) {
//            List<Breakfast> filteredList = new ArrayList<>();
//
//            if (constraint == null || constraint.length() == 0) {
//                filteredList.addAll(dataList);
//            } else {
//                String filterPattern = constraint.toString().toLowerCase().trim();
//
//                for (Breakfast item : dataList) {
//                    if (item.getName().toLowerCase().contains(filterPattern)) {
//                        filteredList.add(item);
//                    }
//                }
//            }
//
//            FilterResults results = new FilterResults();
//            results.values = filteredList;
//
//            return results;
//        }
//
//        @Override
//        protected void publishResults(CharSequence constraint, FilterResults results) {
//            dataList.clear();
//            dataList.addAll((List) results.values);
//            notifyDataSetChanged();
//        }
//    };


}
