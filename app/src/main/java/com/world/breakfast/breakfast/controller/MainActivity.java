package com.world.breakfast.breakfast.controller;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.world.breakfast.breakfast.AboutActivity;
import com.world.breakfast.breakfast.R;
import com.world.breakfast.breakfast.adapter.CustomAdapter;
import com.world.breakfast.breakfast.adapter.RecyclerViewAdapter;
import com.world.breakfast.breakfast.model.Breakfast;
import com.world.breakfast.breakfast.network.GetDataService;
import com.world.breakfast.breakfast.network.RetrofitClientInstance;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import hotchemi.android.rate.AppRate;
import hotchemi.android.rate.OnClickButtonListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


//i am going to add all global variable on bottom of this comment

    private CustomAdapter adapterS;
    private RecyclerView.Adapter<RecyclerView.ViewHolder> adapter;
    private RecyclerView recyclerView;
    static ArrayList<Integer> favoriteList;
    DrawerLayout drawer;
    static int nightM = 1;
    Context context;
    private View view;

    ProgressBar progressBar;
    private FirebaseAnalytics mFirebaseAnalytics;


    /// A banner ad is placed in every 8th position in the RecyclerView.
    public static final int ITEMS_PER_AD = 4;



    // List of banner ads and MenuItems that populate the RecyclerView.
    private List<Object> recyclerViewItems = new ArrayList<>();

    public static InterstitialAd mInterstitialAd;

    /////////////////////////////////////////////////////////////////


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        AppCompatDelegate.setDefaultNightMode(getIntent().getIntExtra("mode", nightM));

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //reActive();

        MobileAds.initialize(this,
                "ca-app-pub-2805622275935178~1067157327");

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-2805622275935178/1069250654");
        mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice("F46327814C9DDA874999EA5C56137236").build());


        //i start coding from her i will add fragment and bottom nav functionality

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new FragmentNewActivity()).commit();

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);

        progressBar = findViewById(R.id.progressBar);
        progressBar = new ProgressBar(getApplicationContext(), null, android.R.attr.progressBarStyleSmall);
        context = this;
        //get data and initialization the recycler view
        Call<List<Breakfast>> call1 = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class).getAllNewRecipes();
        call1.enqueue(new Callback<List<Breakfast>>() {

            @Override
            public void onResponse(Call<List<Breakfast>> call1, Response<List<Breakfast>> response) {
                generateDataList(response.body());
            }
            @Override
            public void onFailure(Call<List<Breakfast>> call1, Throwable t) {
                buildDialog(context).show();
            }
        });

        //on the bottom of this part we will insert the value of favorite list
        favoriteList = new ArrayList<>();
        loadData();


        AppRate.with(this)
                .setInstallDays(1) // default 10, 0 means install day.
                .setLaunchTimes(3) // default 10
                .setRemindInterval(2) // default 1
                .setShowLaterButton(true) // default true
                .setDebug(false) // default false

                .setOnClickButtonListener(new OnClickButtonListener() { // callback listener.
                    @Override
                    public void onClickButton(int which) {
                        Log.d(MainActivity.class.getName(), Integer.toString(which));

                    }
                })
                .monitor();

        // Show a dialog if meets conditions
        AppRate.showRateDialogIfMeetsConditions(this);


        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.navRate:
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=EMFSoft")));
                    // startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                break;

            case R.id.navAbout:
                Intent inte = new Intent(this, AboutActivity.class);
                startActivity(inte);
                break;

            case R.id.switchTheme:
                switch (AppCompatDelegate.getDefaultNightMode()) {
                    case AppCompatDelegate.MODE_NIGHT_YES:
                        nightM = AppCompatDelegate.MODE_NIGHT_NO;
                        reActive(nightM);
                        break;
                    case AppCompatDelegate.MODE_NIGHT_NO:
                        nightM = AppCompatDelegate.MODE_NIGHT_YES;
                        reActive(nightM);
                        break;
                }
                break;

            case R.id.navShare:
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.world.breakfast.breakfast");
                shareIntent.setType("text/*");
                startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.send_to)));
                break;
            case R.id.navSend:
                composeEmail();
                break;
            case R.id.navOtherApp:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=EMFSoft")));
                break;
        }

        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    //i will add my function on bottom of this comment


    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment selectedFragment = null;

            recyclerViewItems.clear();
            adapter.notifyDataSetChanged();

            switch (menuItem.getItemId()) {
                case R.id.nav_New:

                    selectedFragment = new FragmentNewActivity();

                    /*Create handle for the RetrofitInstance interface*/


                    Call<List<Breakfast>> call1 = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class).getAllNewRecipes();
                    call1.enqueue(new Callback<List<Breakfast>>() {

                        @Override
                        public void onResponse(Call<List<Breakfast>> call1, Response<List<Breakfast>> response) {

                            generateDataList(response.body());
                        }

                        @Override
                        public void onFailure(Call<List<Breakfast>> call1, Throwable t) {
                            buildDialog(context).show();
                            //Toast.makeText(MainActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                        }
                    });

                    break;
                case R.id.nav_favorites:
                    selectedFragment = new FragmentFavoritesActivity();


                    Call<List<Breakfast>> call4 = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class).getAllTopRecipes();
                    call4.enqueue(new Callback<List<Breakfast>>() {

                                      @Override
                                      public void onResponse(Call<List<Breakfast>> call, Response<List<Breakfast>> response) {

                                          generateFavoritesDataList(response.body());
                                      }

                                      @Override
                                      public void onFailure(Call<List<Breakfast>> call, Throwable t) {
                                         // Toast.makeText(MainActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                                          buildDialog(context).show();
                                      }
                                  }
                    );

                    break;
                case R.id.nav_top:

                    selectedFragment = new FragmentTopActivity();


                    Call<List<Breakfast>> call3 = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class).getAllTopRecipes();
                    call3.enqueue(new Callback<List<Breakfast>>() {

                        @Override
                        public void onResponse(Call<List<Breakfast>> call3, Response<List<Breakfast>> response) {

                            generateDataList(response.body());


                        }

                        @Override
                        public void onFailure(Call<List<Breakfast>> call3, Throwable t) {
                            buildDialog(context).show();
                            ////Toast.makeText(MainActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                        }
                    });

                    break;


            }

            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();

            return true;
        }
    };

    /*Method to generate List of data using RecyclerView with custom adapter*/
    private void generateDataList(List<Breakfast> breakfasts) {



        recyclerViewItems.addAll(breakfasts);
        addBannerAds();
        loadBannerAds();

        recyclerView = findViewById(R.id.customRecyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        // adapter = new CustomAdapter(this, breakfasts);
        //recyclerView.setAdapter(adapter);

        // Specify an adapter.
        adapter = new RecyclerViewAdapter(this,
                recyclerViewItems);
        recyclerView.setAdapter(adapter);

    }

    /*Method to generate List of data using RecyclerView with custom adapter*/
    private void generateDataListS(List<Breakfast> breakfasts) {


        recyclerView = findViewById(R.id.customRecyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        adapterS = new CustomAdapter(this, breakfasts);
        recyclerView.setAdapter(adapterS);

        adapter.notifyDataSetChanged();

    }

    /*Method to generate favorite List of data using RecyclerView with custom adapter*/
    private void generateFavoritesDataList(List<Breakfast> breakfasts) {


        List<Breakfast> newBreakfasts = new ArrayList<>();


        for (int i = 0; i < breakfasts.size(); i++) {
            for (int j : favoriteList) {
                if (breakfasts.get(i).getId() == j) {
                    newBreakfasts.add(breakfasts.get(i));
                }
            }
        }


        recyclerViewItems.addAll(newBreakfasts);

        addBannerAds();
        loadBannerAds();

        recyclerView = findViewById(R.id.customRecyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        // adapter = new CustomAdapter(this, breakfasts);
        //recyclerView.setAdapter(adapter);

        // Specify an adapter.
        RecyclerView.Adapter<RecyclerView.ViewHolder> adapter = new RecyclerViewAdapter(this,
                recyclerViewItems);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        MenuItem searchItem = menu.findItem(R.id.app_bar_search);
        SearchView searchView =
                (SearchView) searchItem.getActionView();


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
//                if (query != null && query != "".trim()) {
//                    adapter.getFilter().filter(query);
//                    return false;
//                }


                Call<List<Breakfast>> call5 = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class).getSearchRecipes(query);
                call5.enqueue(new Callback<List<Breakfast>>() {

                    @Override
                    public void onResponse(Call<List<Breakfast>> call5, Response<List<Breakfast>> response) {
                        generateDataListS(response.body());
                        adapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onFailure(Call<List<Breakfast>> call5, Throwable t) {
                        buildDialog(context).show();
//                        Toast.makeText(MainActivity.this, "" + t, Toast.LENGTH_LONG).show();
//                        System.out.println(t);
                    }
                });


                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

//                if (newText != null && newText != "".trim()) {
//                    adapter.getFilter().filter(newText);
//                    return false;
//                }


                Call<List<Breakfast>> call5 = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class).getSearchRecipes(newText);
                call5.enqueue(new Callback<List<Breakfast>>() {

                    @Override
                    public void onResponse(Call<List<Breakfast>> call5, Response<List<Breakfast>> response) {
                        generateDataListS(response.body());

                    }

                    @Override
                    public void onFailure(Call<List<Breakfast>> call5, Throwable t) {
                        buildDialog(context).show();
                        ////Toast.makeText(MainActivity.this, "" + t, Toast.LENGTH_SHORT).show();
                    }
                });


                return false;
            }
        });

        return true;
    }


    private void loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences("shared perferences", MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("tasks list", null);
        Type type = new TypeToken<ArrayList<Integer>>() {
        }.getType();
        favoriteList = gson.fromJson(json, type);

        if (favoriteList == null) {
            favoriteList = new ArrayList<>();
        }

    }

    public void reActive(int nightM) {

        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        i.putExtra("mode", nightM);
        finish();
        startActivity(i);
    }

    public void composeEmail() {
        String emailTo[] = {"chef-rc@yandex.com"};
        Intent intentToEmail = new Intent(Intent.ACTION_SENDTO);
        intentToEmail.setData(Uri.parse("mailto:")); // only email apps should handle this
        intentToEmail.putExtra(Intent.EXTRA_EMAIL, emailTo);
        if (intentToEmail.resolveActivity(getPackageManager()) != null) {
            startActivity(intentToEmail);
        }
    }

    /**
     * Adds banner ads to the items list.
     */
    private void addBannerAds() {
        // Loop through the items array and place a new banner ad in every ith position in
        // the items List.
        for (int i = 0; i <= recyclerViewItems.size(); i += ITEMS_PER_AD) {
            final AdView adView = new AdView(MainActivity.this);
            adView.setAdSize(AdSize.MEDIUM_RECTANGLE);
            adView.setAdUnitId("ca-app-pub-2805622275935178/1504364924");
            recyclerViewItems.add(i, adView);
        }
    }

    /**
     * Sets up and loads the banner ads.
     */
    private void loadBannerAds() {
        // Load the first banner ad in the items list (subsequent ads will be loaded automatically
        // in sequence).
        loadBannerAd(0);
    }

    /**
     * Loads the banner ads in the items list.
     */
    private void loadBannerAd(final int index) {

        if (index >= recyclerViewItems.size()) {
            return;
        }

        Object item = recyclerViewItems.get(index);
        if (!(item instanceof AdView)) {
            throw new ClassCastException("Expected item at index " + index + " to be a banner ad"
                    + " ad.");
        }

        final AdView adView = (AdView) item;

        // Set an AdListener on the AdView to wait for the previous banner ad
        // to finish loading before loading the next ad in the items list.
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                // The previous banner ad loaded successfully, call this method again to
                // load the next ad in the items list.
                loadBannerAd(index + ITEMS_PER_AD);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // The previous banner ad failed to load. Call this method again to load
                // the next ad in the items list.
                Log.e("MainActivity", "The previous banner ad failed to load. Attempting to"
                        + " load the next banner ad in the items list.");
                loadBannerAd(index + ITEMS_PER_AD);
            }
        });

        // Load the banner ad.
        adView.loadAd(new AdRequest.Builder().addTestDevice("F46327814C9DDA874999EA5C56137236").build());
    }

    public AlertDialog.Builder buildDialog(Context c) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("No Internet Connection");
        builder.setMessage("You need to have Mobile Data or wifi to access this. Connect your phone to internet or Press QUIT to Exit :");
        builder.setPositiveButton("Connect to WIFI", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }
        }).setNegativeButton("Connect to Mobile Data", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
            }
        }).setNeutralButton("Quit",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });

        return builder;
    }

}
