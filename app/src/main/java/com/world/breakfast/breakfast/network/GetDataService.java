package com.world.breakfast.breakfast.network;


import com.world.breakfast.breakfast.model.Breakfast;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GetDataService {

    @GET("data.php")
    Call<List<Breakfast>> getAllNewRecipes();

    @GET("dataDESC.php")
    Call<List<Breakfast>> getAllTopRecipes();

    @GET("dataSearch.php")
    Call<List<Breakfast>> getSearchRecipes(@Query("key") String key);
}