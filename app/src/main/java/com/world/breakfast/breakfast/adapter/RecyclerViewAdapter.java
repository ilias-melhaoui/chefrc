package com.world.breakfast.breakfast.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;
import com.world.breakfast.breakfast.R;
import com.world.breakfast.breakfast.controller.DetailsActivity;
import com.world.breakfast.breakfast.controller.MainActivity;
import com.world.breakfast.breakfast.model.Breakfast;

import java.util.List;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


/**
 * The {@link RecyclerViewAdapter} class.
 * <p>The adapter provides access to the items in the {@link MenuItemViewHolder}
 * or the {@link AdViewHolder}.</p>
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    // A menu item view type.
    private static final int MENU_ITEM_VIEW_TYPE = 0;

    // The banner ad view type.
    private static final int BANNER_AD_VIEW_TYPE = 1;

    // An Activity's Context.
    private final Context context;

    // The list of banner ads and menu items.
    private final List<Object> recyclerViewItems;

//    /**
//     * For this example app, the recyclerViewItems list contains only
//     * {@link MenuItem} and {@link AdView} types.
//     */

    public RecyclerViewAdapter(Context context, List<Object> recyclerViewItems) {
        this.context = context;
        this.recyclerViewItems = recyclerViewItems;
    }

    /**
     * The {@link MenuItemViewHolder} class.
     * Provides a reference to each view in the menu item view.
     */
    public class MenuItemViewHolder extends RecyclerView.ViewHolder {

        public final View mView;



        public TextView name, ingredients, instructions, id, likes;
        ImageView image;
        CardView cardView;

        MenuItemViewHolder(View itemView) {
            super(itemView);
            mView = itemView;

            cardView = itemView.findViewById(R.id.card_view_friend);
            name = itemView.findViewById(R.id.tvName);
            ingredients = itemView.findViewById(R.id.tvIngredients);
            instructions = itemView.findViewById(R.id.tvInstructions);
            likes = itemView.findViewById(R.id.tvLikes);
            id = itemView.findViewById(R.id.tvId);
            image = itemView.findViewById(R.id.image);


        }
    }

    /**
     * The {@link AdViewHolder} class.
     */
    public class AdViewHolder extends RecyclerView.ViewHolder {

        AdViewHolder(View view) {
            super(view);
        }
    }

    @Override
    public int getItemCount() {
        return recyclerViewItems.size();
    }

    /**
     * Determines the view type for the given position.
     */
    @Override
    public int getItemViewType(int position) {
        return (position % MainActivity.ITEMS_PER_AD == 0) ? BANNER_AD_VIEW_TYPE
                : MENU_ITEM_VIEW_TYPE;
    }

    /**
     * Creates a new view for a menu item view or a banner ad view
     * based on the viewType. This method is invoked by the layout manager.
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case MENU_ITEM_VIEW_TYPE:
                View menuItemLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(
                        R.layout.custom_row, viewGroup, false);
                return new MenuItemViewHolder(menuItemLayoutView);
            case BANNER_AD_VIEW_TYPE:
                // fall through
            default:
                View bannerLayoutView = LayoutInflater.from(
                        viewGroup.getContext()).inflate(R.layout.banner_ad_container,
                        viewGroup, false);
                return new AdViewHolder(bannerLayoutView);
        }
    }

    /**
     * Replaces the content in the views that make up the menu item view and the
     * banner ad view. This method is invoked by the layout manager.
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case MENU_ITEM_VIEW_TYPE:
                MenuItemViewHolder menuItemHolder = (MenuItemViewHolder) holder;
                final Breakfast menuItem = (Breakfast) recyclerViewItems.get(position);

                menuItemHolder.name.setText(menuItem.getName());
                menuItemHolder.instructions.setText(menuItem.getInstructions());
                menuItemHolder.ingredients.setText(menuItem.getIngredients());
                menuItemHolder.likes.setText(String.valueOf(menuItem.getLikes()));
                menuItemHolder.id.setText(String.valueOf(menuItem.getId()));
                Picasso.get().load(menuItem.getImage()).placeholder(R.drawable.ic_launcher_background).into(menuItemHolder.image);

                menuItemHolder.cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (MainActivity.mInterstitialAd.isLoaded()) {
                            MainActivity.mInterstitialAd.show();
                            MainActivity.mInterstitialAd.setAdListener(new AdListener() {
                                @Override
                                public void onAdFailedToLoad(int i) {
                                    super.onAdFailedToLoad(i);
                                    Intent intent = new Intent(context, DetailsActivity.class);
                                    intent.putExtra("name", menuItem.getName());
                                    intent.putExtra("image", menuItem.getImage());
                                    intent.putExtra("likes", menuItem.getLikes());
                                    intent.putExtra("id", menuItem.getId());
                                    intent.putExtra("instructions", menuItem.getInstructions());
                                    intent.putExtra("ingredients", menuItem.getIngredients());
                                    context.startActivity(intent);

                                }

                                @Override
                                public void onAdLeftApplication() {
                                    super.onAdLeftApplication();


                                }


                                @Override
                                public void onAdOpened() {
                                    super.onAdOpened();
                                }

                                @Override
                                public void onAdLoaded() {
                                    super.onAdLoaded();
                                }

                                @Override
                                public void onAdClicked() {
                                    super.onAdClicked();
                                }

                                @Override
                                public void onAdImpression() {
                                    super.onAdImpression();
                                }

                                @Override
                                public void onAdClosed() {
                                    Intent intent = new Intent(context, DetailsActivity.class);
                                    intent.putExtra("name", menuItem.getName());
                                    intent.putExtra("image", menuItem.getImage());
                                    intent.putExtra("likes", menuItem.getLikes());
                                    intent.putExtra("id", menuItem.getId());
                                    intent.putExtra("instructions", menuItem.getInstructions());
                                    intent.putExtra("ingredients", menuItem.getIngredients());
                                    context.startActivity(intent);
                                }
                            });
                        } else {
                            Intent intent = new Intent(context, DetailsActivity.class);
                            intent.putExtra("name", menuItem.getName());
                            intent.putExtra("image", menuItem.getImage());
                            intent.putExtra("likes", menuItem.getLikes());
                            intent.putExtra("id", menuItem.getId());
                            intent.putExtra("instructions", menuItem.getInstructions());
                            intent.putExtra("ingredients", menuItem.getIngredients());
                            context.startActivity(intent);
                        }




                    }
                });
                break;
            case BANNER_AD_VIEW_TYPE:
                // fall through
            default:
                AdViewHolder bannerHolder = (AdViewHolder) holder;
                AdView adView = (AdView) recyclerViewItems.get(position);
                ViewGroup adCardView = (ViewGroup) bannerHolder.itemView;
                // The AdViewHolder recycled by the RecyclerView may be a different
                // instance than the one used previously for this position. Clear the
                // AdViewHolder of any subviews in case it has a different
                // AdView associated with it, and make sure the AdView for this position doesn't
                // already have a parent of a different recycled AdViewHolder.
                if (adCardView.getChildCount() > 0) {
                    adCardView.removeAllViews();
                }
                if (adView.getParent() != null) {
                    ((ViewGroup) adView.getParent()).removeView(adView);
                }

                // Add the banner ad to the ad view.
                adCardView.addView(adView);
        }
    }

}
